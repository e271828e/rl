

from enum import IntEnum
import gym
import numpy as np
#the maze is represented by a 2D grid of ints, which we could define through an
#enumeration. each int value represents a type of cell (free, wall)

#para ver si una celda esta comunicada con

#the state space is the set of grid cells in which the agent may be at a given
#step. how do we represent a specific state? for the environment, the most
#straightforward way is to use a (row, column) tuple. this is also the
#observation output by the environment at each step
#
# however, for an agent using a tabular algorithm we need to arrange each of
# these tuples along one dimension of a 2D table, that is, we need to map the
# (row, col) tuple to a single index.if state_2D[0] represents row and
# state_2D[1] column, then state_1D = state_2D[0] * n_cols + state_2D[1]. but
# this is exactly what is achieved by np.ravel_multi_index(state_2D, grid.shape)

# an alternative is to represent the state-action values not in a 2D table but
# in a 3D tensor, with dimensions (row, col, action)


class Tile:

    def __init__(self):
        self.location = (0, 0)

        # Tile grid: 2D array of tiles. Tiles can be subclassed
        # Tile attributes: Coordinates, representation (textual, sprite...), walkable,
        # weight/penalty/reward, effect, next_state, get_reward(), get_state(),
        # get_neighbors() (no, porque esto requiere conocer el tamano del grid)

        pass

    def render():
        pass

    def get_reward(self):
        pass

    def reachable_from(self, prev_loc) -> bool:
        #a portal tile with the portal placed on the NORTH wall (looking SOUTH)
        #will not be reachable from the tile NORTH of its own location
        #a wall tile will not be reachable from any location
        pass

    def target_location(self, action):
        #for a regular corridor tile this would be:
        return next_loc
        #however, for a portal tile, the target location, if the portal is
        #placed in the north wall, will be the

#the game manager knows the agent's location and the grid. the agent takes an
#action. the game manager queries the current tile for the effect of the
#player's action. this will typically lead the player to a target location. the
#manager queries the tile in the target location to determine if it is reachable
#from the agent's location. if it is not, the agent remains where he is. if it
#is, the game manager moves the agent to the target tile.

#what do i need from the tiles for the graph generation:
#the graph generator gets the grid as an argument
#the graph generator loops over walkable tiles. for each, it takes an action,
#which yields a next_location. it then asks the tile at next_location if it
#is reachable from current_location. if it is, it adds an edge from the current
#location to next_location, and its weight is the reward associated to the
#next_location tile. if it is not, it does nothing and continues with the next
#action.

#Mejor prescindir de lo del hielo. Lo que puedo anadir es una casilla stream,
#que hace las veces de slippery.
#directions = (1,0), (-1,0), (0,1), (0,-1)
#need to build a probability distribution for delta_loc with four outcomes:
# (STREAM_DIR, -STREAM_DIR, STREAM_RIGHT, STREAM_LEFT)
#If action == STREAM_DIR
#PERO OJO, que entonces ya no se puede resolver por Dijkstra

###################### maze #####################

class Maze:
    #automatic maze generator method goes here
    #maze representation defined here
    #this should be passed to MazeEnv as an constructor argument. it holds the
    #representation of the maze, but it doesn't know anything about actions,
    #observations, states or rewards.
    #defines n_rows, n_cols, start, goal
    def __init__(self, layout: np.ndarray, start: tuple,
                 goal: tuple):

        self.layout = layout.astype(int)
        self.shape = layout.shape
        self.start = start
        self.goal = goal

    def is_inside(self, location: tuple) -> bool:
        return ((0 <= location[0] < self.shape[0]) and
                (0 <=location[1] < self.shape[1]))

    def is_wall(self, location: tuple) -> bool:
        try:
            return self.layout[location]
        except IndexError:
            print("Error: Location is outside of maze")

    def is_free(self, location: tuple) -> bool:
        try:
            return not self.layout[location]
        except IndexError:
            print("Error: Location is outside of maze")

    def is_walkable(self, location: tuple) -> bool:
        return (self.is_inside(location) and self.is_free(location))

    def is_goal(self, location: tuple) -> bool:
        return self.goal == location


class MazeEnv(gym.Env):

    class Actions(IntEnum):
        UP = 0
        DOWN = 1
        LEFT = 2
        RIGHT = 3


    class ActionSpace(gym.spaces.Discrete):

        def __init__(self):
            super().__init__(4)

        def sample(self):
            return MazeEnv.Actions(super().sample())


    def __init__(self, maze, time_reward=-0.1, collision_reward=0,
                 final_reward=0):

        super().__init__()
        self._maze = maze
        self._time_reward = time_reward
        self._collision_reward = collision_reward
        self._final_reward = final_reward
        self._state = self._maze.start
        self.action_space = self.ActionSpace()
        self.observation_space = gym.spaces.Tuple((
            gym.spaces.Discrete(self._maze.shape[0]),
            gym.spaces.Discrete(self._maze.shape[1])))


    def reset(self):
        self._state = np.array(self._maze.start)
        return self._state


    def step(self, action: MazeEnv.Actions):

        reward = self._time_reward

        if action is not None:

            if not self.action_space.contains(action):
                raise ValueError("Invalid action")

            if action == self.Actions.UP:
                delta = (-1, 0)
            elif action == self.Actions.DOWN:
                delta = (1, 0)
            elif action == self.Actions.LEFT:
                delta = (0, -1)
            else: #action == self.Actions.RIGHT:
                delta = (0, 1)

            next_location = (self._state[0] + delta[0],
                             self._state[1] + delta[1])

            if self._maze.is_walkable(next_location):
                self._state = next_location
                print(f"Going {self.Actions(action).name}")
            else:
                print(f"Can't go {self.Actions(action).name}")
                reward += self._collision_reward

        if self._maze.is_goal(self._state):
            done = True
            reward = self._final_reward
        else:
            done = False

        observation = self._state
        info = None

        return observation, reward, done, info

    def render(self):
        pass


if __name__ == "__main__":
    maze_layout = np.array([
        [1, 0, 1, 0, 0],
        [0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 1]
    ])
    maze1 = Maze(maze_layout, start=(0, 1), goal=(0, 3))
    env1 = MazeEnv(maze1)
    env1._maze.layout

    for _ in range(10):
        env1.step(env1.action_space.sample())