from __future__ import annotations
from collections import namedtuple, deque

#a digraph implementation can be used to represent an undirected graph by
#adding two edges with opposite directions between each vertex pair

Edge = namedtuple('Edge', ['tail_id', 'head_id', 'weight'])

class _Vertex:

    def __init__(self, data=None):

        self._inward_edges = {}
        self._outward_edges = {}
        self.data = data

    def add_inward_edge(self, edge_id, edge: Edge):
        self._inward_edges[edge_id] = edge

    def add_outward_edge(self, edge_id, edge: Edge):
        self._outward_edges[edge_id] = edge

    def remove_inward_edge(self, edge_id):
        del self._inward_edges[edge_id]

    def remove_outward_edge(self, edge_id):
        del self._outward_edges[edge_id]

    def get_inward_edges(self):
        return self._inward_edges

    def get_outward_edges(self):
        return self._outward_edges

    def connected_to(self, vertex_id):
        for edge in self._outward_edges.values():
            if edge.head_id == vertex_id:
                return True
        return False

    def connected_from(self, vertex_id):
        for edge in self._inward_edges.values():
            if edge.tail_id == vertex_id:
                return True
        return False

    def __str__(self):
        return (f"Vertex with Data: {str(self.data)}"
                " | Inward edges: "
                f"{list(edge_id for edge_id in self._inward_edges)}"
                " | Outward edges: "
                f"{list([edge_id for edge_id in self._outward_edges])}")


class Digraph:

    def __init__(self, allow_loops=True):

        self._vertices = {}
        self._edges = {}
        self._allow_loops = allow_loops

    def add_vertex(self, vertex_id, vertex_data=None):

        #id must be hashable, but not necessarily equal to vertex.data
        new_vertex = _Vertex(vertex_data)
        try:
            self._vertices[vertex_id] = new_vertex
        except TypeError:
            print("Vertex ID must be hashable")

    def add_edge(self, tail_id, head_id, weight=0, edge_id=None):

        #leaving edge_id unspecified for all connections ensures no multiple
        #edges will exist, because each tail and head combination will be
        #represented stored with the unique key (tail_id, head_id)
        if edge_id is None:
            edge_id = (tail_id, head_id)

        if tail_id == head_id and not self._allow_loops:
            raise ValueError("No loops allowed")

        if edge_id in self._edges:
            raise ValueError("An edge with the same ID already exists")

        new_edge = Edge(tail_id, head_id, weight)
        try:
            self._vertices[tail_id].add_outward_edge(edge_id, new_edge)
            self._vertices[head_id].add_inward_edge(edge_id, new_edge)
            self._edges[edge_id] = new_edge
        except KeyError:
            print("Non-existent vertex ID")

    def remove_vertex(self, vertex_id):

        vertex = self._vertices[vertex_id]

        for edge_id, edge in vertex.get_inward_edges().items():
            tail = self._vertices[edge.tail_id]
            tail.remove_outward_edge(edge_id)
            self._edges.pop(edge_id)

        for edge_id, edge in vertex.get_outward_edges().items():
            head = self._vertices[edge.head_id]
            head.remove_inward_edge(edge_id)
            self._edges.pop(edge_id)

        del self._vertices[vertex_id]

    def remove_edge(self, edge_id):

        edge = self._edges[edge_id]

        tail = self._vertices[edge.tail_id]
        tail.remove_outward_edge(edge_id)

        head = self._vertices[edge.head_id]
        head.remove_inward_edge(edge_id)

        self._edges.pop(edge_id)

    def get_vertices(self, vertex_ids=None):
        if vertex_ids is None:
            return self._vertices.items()
        try:
            return ((v, self._vertices[v]) for v in vertex_ids
                    if v in self._vertices)
        except TypeError:
            print("Input must be an iterable of vertex IDs")

    def get_edges(self, edge_ids=None):
        if edge_ids is None:
            return self._edges.items()
        try:
            return ((e, self._edges[e]) for e in edge_ids
                    if e in self._edges)
        except TypeError:
            print("Input must be an iterable of edge IDs")


    def breadth_first_search(self, v_id):
        #uses a set to store explored vertices, because membership test is O(1).
        #an O(1) alternative would be a dict, with vertex_id as keys and a
        #membership bool as values. an array would only be O(1) if the vertices
        #were all identified by a sequential number, which is not the case.
        #adding w at the tail of the queue means that we will eventually
        #continue exploring from it, but after all the other vertices directly
        #reachable from v (that is, those at the same level), which are at the
        #front of the queue, have already been explored

        if v_id not in self._vertices:
            return None

        explored = set([v_id]) #initialize set of explored vertex IDs
        queue = deque([v_id])
        while queue:
            v_id = queue.pop()
            v = dict(self.get_vertices((v_id,)))[v_id]
            for edge in v.get_outward_edges().values():
                w_id = edge.head_id
                if not w_id in explored:
                    explored.add(w_id)
                    queue.appendleft(w_id)
        return explored

    def depth_first_search(self, v_id):

        if v_id not in self._vertices:
            return None

        explored = set()
        queue = deque([v_id])
        while queue:
            v_id = queue.pop()
            if not v_id in explored:
                explored.add(v_id)
                v = dict(self.get_vertices((v_id,)))[v_id]
                for edge in v.get_outward_edges().values():
                    w_id = edge.head_id
                    queue.append(w_id)

        return explored


if __name__ == "__main__":
    pass