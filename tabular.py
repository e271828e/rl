import numpy as np
from abc import ABC, abstractmethod, abstractproperty
import gym
import gridworld as gw


class Policy(ABC):

    def __init__(self, rng_seed: int=None):

        #when called with parameter None, default_rng will pull an unpredictable
        #entropy from the OS and use it as seed. otherwise, it will use the
        #specified seed. use this to achieve repeatability between runs.
        self._rng = np.random.default_rng(rng_seed)

    @abstractmethod
    def action_probs(self, state: int):
        pass

    @abstractproperty
    def n_actions(self):
        pass

    def sample(self, state: int):
        return self._rng.choice(self.n_actions, p=self.action_probs(state))

    def set_rng_seed(self, seed: int):
        self._rng = np.random.default_rng(seed)

class epsGreedyPolicy(Policy):

    def __init__(self, q: np.ndarray, epsilon: float=0, rng_seed: int=None):

        super().__init__(rng_seed)
        self._q = q
        self._ε = self.epsilon = epsilon

    @property
    def n_actions(self):
        return self._q.shape[1]

    @property
    def epsilon(self):
        return self._ε

    @epsilon.setter
    def epsilon(self, epsilon: float):
        if epsilon <0 or epsilon > 1:
            raise ValueError("Epsilon must be within [0, 1]")
        self._ε = epsilon

    def action_probs(self, state: int):
        probs = np.full(self.n_actions, self._ε / self.n_actions)
        probs[self._q[state, :].argmax()] += 1 - self._ε
        return probs

class DirectPolicy(Policy):

    def __init__(self, p: np.ndarray, rng_seed: int=None):

        super().__init__(rng_seed)
        if π.ndim != 2:
            raise TypeError("Probability matrix must be a 2D array")
        if not np.allclose(p.sum(1), 1):
            raise ValueError("Every probability matrix row must sum to 1")
        self._p = p

    @property
    def n_actions(self):
        return self._p.shape[1]

    def action_probs(self, state: int):
        return self._p[state, :]

class RandomPolicy(Policy):

    def __init__(self, n_actions: int, rng_seed: int=None):

        super().__init__(rng_seed)
        self._n_actions = n_actions

    @property
    def n_actions(self):
        return self._n_actions

    def action_probs(self, state: int):
        return np.full(self._n_actions, 1 / self._n_actions)


def TD0(env: gym.Env, π: Policy, α: float=0.1):

#por ahora, constant step size
#convergence metric: norm of the deltaV between steps

#constant step size vs 1/n vs exponential recency weighted (Sutton).
#for the exponential case, we need to keep track of the current step size
#for each state. the step size is given by alpha_exp_n = alpha / theta_n,
#where theta_n is an exponential decay factor computed as: theta_n =
#theta_n-1 + alpha * (1 - theta_n-1), n is the number of times a state has
#been visited, and theta_0 = 0.

# to implement this we create an array of theta values. whenever
#we visit a state:
#theta[state] = theta[state] + alpha * (1 - theta[state])
#alpha_exp = alpha / theta[state]
#V[state] = V[state] + alpha_exp * (reward + gamma * V[state_next] - V[state])

#but this cannot be applied directly to n-step methods because the update
#target does not correspond to any specific state, but a sequence of states,
#so we cannot set alpha_exp for this target in the same way we did here.


#TD0, the policy to evaluate is frozen in a matrix. it is not greedy with
#respect to any particular action value function. we could specify it directly

#EPSILON IS NOT REDUCED PER STEP, BUT PER EPISODE. it is done by the agent or
#algorithm, not the policy

#for each policy pi, we have a value function vpi(s). and we also have an action
#value function qpi(s, a), which is the expected return after taking action a
#and then following policy pi. both are related, obviously.

#now, in on-policy learning, we have only one policy (generally, eps greedy or
#tapered eps greedy) and we are learning the action value function for that
#policy, as we act with respect to it. there is a consistency between the policy
#we follow and the resulting q. therefore, once the method converges, the
#resulting q will depend on the epsilon we have set for our policy (unless we
#reduce epsilon over episodes).

#in Q learning, there is also only one action value function being learned: q*,
#which corresponds to the optimal (and therefore completely greedy) policy.
#however, we are not picking actions greedily with respect to q*, but from
#another acting policy, which may be an eps greedy policy based on q*, or any
#other (but it must be soft in order for all state-action pairs to be eventually
#visited). note that in the first case, the acting policy changes along with the
#learned q during execution. once the process converges, q will be q*. because
#we are not following q*, we are not reaping the maximum rewards we could if we
#were. not only that, but it may be the case that we are reaping even less
#rewards that if the q function we learned actually corresponded to the
#(non-greedy) action policy we are following. see cliff example in Sutton.

#practically, for Sarsa we instantiate an action value q_learn(to be learned)
#and one eps greedy policy to act with respect to it. for Q learning we
#instantiate an action value q_learn (to be learned as q*), and one policy,
#which may be epsilon greedy with respect to q_learn or independent (directly
#specified, for instance)

# Como SARSA en on policy, la propia policy que aprende tiene en cuenta el hecho
# de que estamos imponiendo una cierta exploración en la policy, que por tanto
# nunca va a ser 100% greedy, y que de vez en cuando va a haber una acción que
# podría aleqtoriamente acercarnos al abismo. De manera natural la policy
# resultante nos aleja un poco del borde para acomodar esa aleatoriedad.

# En cambio, Q learning aprende la policy que maximiza q, que lógicamente es la
# que va junto al borde, pero no tiene en cuenta que esa no es exactamente la
# policy que aplicamos, sino que mantenemos siempre un cierto grado de
# aleatoriedad para explorar. Y por ello de vez en cuando cae al abismo y las
# rewards acumuladas son menores que con SARSA

#for an off policy learning,

if __name__ == "__main__":
    dgenv = gw.GridWorldEnv()
    print(dgenv)
    dgenv._state
    out = dgenv.step(dgenv.Actions.DOWN)
    print(out)
    pi1 = epsGreedyPolicy(np.random.random([3, 4]), epsilon=0, rng_seed=123)
    pi2 = DirectPolicy(np.full([2, 4], 0.25))