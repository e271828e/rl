import numpy as np
from enum import IntEnum
import gym

#pylint: disable=invalid-name

"""Defines a grid world and a wrapping Gym Environment."""

class GridWorld:

    _n_actions = 4

    class Actions(IntEnum):
        UP = 0
        DOWN = 1
        LEFT = 2
        RIGHT = 3

    motions = {
        Actions.UP: np.array((-1, 0), int),
        Actions.DOWN: np.array((1, 0), int),
        Actions.LEFT: np.array((0, -1), int),
        Actions.RIGHT: np.array((0, 1), int)
    }

    def __init__(self, shape=(4, 4), terminal_states=None, reward_matrix=None,
                 epsilon=0, rng_seed=None):

        self._shape = shape
        self._n_rows = shape[0]
        self._n_cols = shape[1]
        self._n_states = self._n_rows * self._n_cols
        self._rng = np.random.default_rng(rng_seed)

        #must be set before terminal states so we don't overwrite the 0 rewards
        if reward_matrix is None:
            reward_matrix = np.full((self._n_states, self._n_actions), -1)
            #punish entering the bottom right corner from the left
            reward_matrix[self.to_state(self.corner("BR") +
                                        self.motions[self.Actions.LEFT]),
                          self.Actions.RIGHT] = -3
        if reward_matrix.shape != (self._n_states, self._n_actions):
            raise TypeError(
                "Reward matrix must be of shape (n_states, n_actions)")
        self._reward_matrix = reward_matrix

        #epsilon determines the minimum probability of selecting an action
        #randomly. epsilon = 0 always selects the desired action. epsilon = 1
        #selects all actions with equal probability, regardless of the desired
        #action. the definition matches that used in epsilon-greedy policies.
        if epsilon < 0 or epsilon > 1:
            raise ValueError("Epsilon must be within [0,1]")
        self._epsilon = epsilon

        #we need to define at least one reachable terminal state. otherwise, the
        #agent would move through the grid gathering rewards forever. in the
        #context of iterative methods, the policy will not converge and the
        #state values estimates will diverge without bounds. a grid with no
        #terminal states can still be forced by passing an empty iterator as a
        #terminal_states value
        if terminal_states is None:
            terminal_states = (self.to_state(self.corner("BR")),)
        self._terminal_states = set()
        self.add_terminal_states(terminal_states)

    def corner(self, label):
        if label == "TL":
            return np.array((0, 0), int)
        if label == "BL":
            return np.array((self._n_rows-1, 0), int)
        if label == "TR":
            return np.array((0, self._n_cols-1), int)
        if label == "BR":
            return np.array((self._n_rows-1, self._n_cols-1), int)

        raise ValueError("Invalid corner identifier")

    def to_state(self, coords: tuple) -> int:
        return np.ravel_multi_index(coords, self._shape)

    def to_coords(self, state: int) -> np.ndarray:
        return np.array(np.unravel_index(state, self._shape), int).transpose()

    def valid_coords(self, coords: tuple) -> bool:
        return (coords[0]>=0 and coords[0]<self._n_rows and
            coords[1]>=0 and coords[1]<self._n_cols)

    def step(self, state: int, action: Actions) -> int:

        if state in self._terminal_states:
        #for a terminal state, the next state is itself regardless of the chosen
        #action, and its reward is zero.
            r_next = 0
            s_next = state
        else:
            action_probs = np.full(self._n_actions,
                                   self._epsilon / self._n_actions)
            action_probs[action] += 1 - self._epsilon
            selected_action = self._rng.choice(self._n_actions, p=action_probs)
            r_next = self._reward_matrix[state, selected_action]
            new_coords = tuple(np.add(self.to_coords(state),
                                      self.motions[selected_action]))
            if self.valid_coords(new_coords):
                s_next = self.to_state(new_coords)
            else:
                s_next = state

        return r_next, s_next

    def add_terminal_states(self, new_states: tuple):
        if new_states:
            self._terminal_states.update(set(new_states))
            self._reward_matrix[new_states, :] = 0

    def set_state_rewards(self, states: int, actions: tuple,
                          rewards: tuple):
        if self._terminal_states.intersection(set(states)):
            raise ValueError("Cannot set rewards for terminal states")
        try:
            self._reward_matrix[np.ix_(states, actions)] = rewards
        except IndexError:
            print("Selected states or actions do not exist")
        except ValueError:
            print("Size mismatch between the selected states and actions "
                  "and the assigned rewards")

    def set_rng_seed(self, rng_seed: int):
        self._rng = np.random.default_rng(rng_seed)

    @property
    def terminal_states(self):
        return self._terminal_states

    @property
    def reward_matrix(self):
        return self._reward_matrix

    @property
    def n_states(self):
        return self._n_states

    @property
    def n_actions(self):
        return self._n_actions

    @property
    def shape(self):
        return self._shape


class GridWorldEnv(gym.Env):

    metadata = {"render.modes": ["ansi"]}

    Actions = GridWorld.Actions

    class ActionSpace(gym.spaces.Discrete):

        def __init__(self):
            super().__init__(4)

        def sample(self):
            return GridWorld.Actions(super().sample())


    def __init__(self, grid=GridWorld(), initial_coords=(0, 0)):

        super().__init__()
        self._grid = grid
        self._initial_coords = initial_coords
        self._state = self._grid.to_state(self._initial_coords)
        self.action_space = self.ActionSpace()
        self.observation_space = gym.spaces.Tuple((
            gym.spaces.Discrete(self._grid._shape[0]),
            gym.spaces.Discrete(self._grid._shape[1])))

    def reset(self):
        #by default, this does not reset the grid's rng seed, because in general
        #different runs should yield different random sequences
        #to achieve repeatability, use the set_rng_seed method
        self._state = self._grid.to_state(self._initial_coords)

    def step(self, action: GridWorld.Actions=None):

        if action is None:
            reward = 0
        elif self.action_space.contains(action):
            reward, self._state = self._grid.step(self._state, action)
        else:
            raise ValueError("Invalid action")

        done = self._state in self._grid.terminal_states
        observation = self._state
        info = None

        return observation, reward, done, info

    def render(self, mode="ansi"):
        pass

    def set_rng_seed(self, seed):
        self._grid.set_rng_seed(seed)

    @property
    def coords(self):
        return self._grid.coords(self._state)

    @property
    def state(self):
        return self._state


if __name__=="__main__":

    grid_test = GridWorld((4, 4), terminal_states=()) #enforce no terminal states
    for c in {"TL", "TR", "BL", "BR"}:
        grid_test.add_terminal_states((grid_test.to_state(grid_test.corner(c)),))
