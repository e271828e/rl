#%%
import time
import gym
from gym.envs.toy_text import frozen_lake

#%%
map_random = frozen_lake.generate_random_map(size=8, p=0.6)
map_predef_8 = frozen_lake.MAPS["8x8"]
map_predef_4 = frozen_lake.MAPS["4x4"]
env = frozen_lake.FrozenLakeEnv(desc=map_predef_8, is_slippery=False)
observation = env.reset()
env.render()
env.step(0)
env.render()
env.step(1)
env.render()
observation, reward, done, info = env.step(2)
env.render()

# ipdb.set_trace()

#%%
for i_episode in range(2):
    observation = env.reset()
    for t in range(10):
        env.render()
        print(observation)
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        if done:
            print("Episode finished after {} timesteps".format(t+1))
            break
env.close()

# %%

class PolicyEvaluationAgent:

    def __init__(self, n_states):