from gridworld import GridWorld
import numpy as np


def gen_policy_iteration(grid: GridWorld, v_iter=10, π_iter_max=20, gamma=1.0):

    #this implements Generalized Policy Iteration for a deterministic grid
    #world. setting v_iter = 1 yields value iteration (one sweep through the
    #states per policy improvement step). setting v_iter to a high value allows
    #the value and action value functions to converge to their true values
    #corresponding to the current policy before improving the policy

    if not grid.terminal_states:
        print("Warning: Grid has no terminal states!")

    #initialize random policy
    π = np.full((grid.n_states, grid.n_actions), 1/grid.n_actions)
    v, q = evaluate_policy(grid, π, v=None, n_iter=v_iter)

    for i in range(π_iter_max):
        print(f"\nIteration {i}:")
        best_actions = π.argmax(1)
        v, q = evaluate_policy(grid, π, v=v, n_iter=v_iter, gamma=gamma)
        π = get_greedy_policy(q)
        best_actions_prev = best_actions
        best_actions = π.argmax(1)
        print(best_actions.reshape(grid.shape))
        if np.array_equal(best_actions, best_actions_prev):
            print("\nPolicy iteration converged, terminating")
            break
    else:
        print("Policy iteration did not converge")

def evaluate_policy(grid: GridWorld, π, v=None, n_iter=10, gamma=1.0):

    if v is None:
        v = np.zeros(grid.n_states)
    q = np.zeros((grid.n_states, grid.n_actions))

    for _ in range(n_iter):
        # v_0 = v.copy() #this could be used instead ofin-place update
        for s in range(grid.n_states):
            for a in range(grid.n_actions):
                r_next, s_next = grid.step(s, a)
                q[s, a] = r_next + gamma * v.item(s_next)
            v[s] = np.sum(π[s,:] * q[s,:])
    return v, q

def get_greedy_policy(q):
    """Given a state-action value matrix q, returns the corresponding
    optimal policy, which simply assigns probability 1 to the optimal action
    for each state"""

    π = np.zeros_like(q)
    π[np.arange(q.shape[0]), q.argmax(1)] = 1
    return π


if __name__ == "__main__":

    #must be deterministic!!
    g1 = GridWorld((8, 10), epsilon=0)
    for c in {"TL", "TR", "BL", "BR"}:
        g1.add_terminal_states((g1.to_state(g1.corner(c)),))

    g1.set_state_rewards((g1.to_state(g1.corner("TR") + 2*g1.motions[g1.Actions.DOWN]),),
                         (g1.Actions.UP, g1.Actions.DOWN),
                         -4)

    gen_policy_iteration(g1)
