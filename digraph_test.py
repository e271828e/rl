#%%
from digraph import Digraph

g1 = Digraph()
for v in ((0,0), (0,1), (1,0), (1,1)):
    g1.add_vertex(v)
g1.add_edge((0, 0), (1, 0), edge_id=1, weight=4.51)
g1.add_edge((0, 0), (0, 1), edge_id=2, weight=2.124)
g1.add_edge((1, 0), (0, 1), edge_id=None, weight=14.1)
g1.add_edge((0, 1), (1, 1), weight=24.1)
g1.add_edge((1, 1), (1, 1), edge_id="loop1", weight=2.)
g1.add_edge((0, 1), (0, 1), edge_id="loop2", weight=2.)
for v in g1.get_vertices():
    print(v)
print()
for e in g1.get_edges():
    print(e)
print()

g1.remove_vertex((0,1))
for v in g1.get_vertices():
    print(v)
for e in g1.get_edges():
    print(e)
print()

#%%
g2 = Digraph()
for v in "sabcde":
    g2.add_vertex(v)

for (edge, vertices) in enumerate(["sa", "sb", "ac", "bc", "ce", "cd", "db"]):
    g2.add_edge(*vertices, edge_id=edge)

for e in g2.get_edges((1,2,4)):
    print(e)

dict(g2.get_edges((1,)))[1]

print(g2.breadth_first_search("s"))
print(g2.breadth_first_search("b"))
print(g2.breadth_first_search("d"))
print()
print(g2.depth_first_search("s"))
print(g2.depth_first_search("b"))
print(g2.depth_first_search("d"))

# %%
